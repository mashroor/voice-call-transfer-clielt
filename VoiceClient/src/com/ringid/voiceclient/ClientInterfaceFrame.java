package com.ringid.voiceclient;

import com.ringid.utils.Constants;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

import com.ringid.utils.*;

/**
 *
 * @author Anwar
 */
public class ClientInterfaceFrame extends JFrame {
    
    JPanel contentPane;
    JPanel jPanel1 = new JPanel();
    JPanel jPanel2 = new JPanel();
    JTextField userIdentityInput = new JTextField();
    JTextField friendIdentityInput = new JTextField();
    JLabel usernameLabel = new JLabel();
    JLabel mesageToLabel = new JLabel();
    JButton sendRegButton = new JButton();
    JButton callButton = new JButton();
    JButton byeButton = new JButton();
    JButton answerButton = new JButton();
    JButton busyButton = new JButton();
    JButton cancelButton = new JButton();
    JButton leaveButton = new JButton();
    JButton quitButton = new JButton();
    
    
    JLabel transferUsernameLabel = new JLabel();/////////////////////////////////////////////////////////////////////////////
    JTextField transferUserIdentityInput = new JTextField();/////////////////////////////////////////////////////////////////
    JButton sendTransferButton = new JButton(); /////////////////////////////////////////////////////////////////////////////////
    
    JButton answerTransferButton = new JButton(); /////////////////////////////////////////////////////////////////////////////////
    JButton busyTransferButton = new JButton(); /////////////////////////////////////////////////////////////////////////////////
    
    
    boolean isTransferred = false;
    
    JScrollPane jScrollPane1 = new JScrollPane();
    JTextArea displayInGUI = new JTextArea();
    static final int BUFFER_SIZE = 516;
    DatagramSocket voiceSocket;
    DatagramPacket finalPacket;
    long userIdentity;
    long friendIdentity;
    static long toTransferFriendIdentity;
    long transferUserIdentity;
    int packetType;
    byte[] finalBytePacket;

    /**
     * Construct the frame
     */
    public ClientInterfaceFrame(DatagramSocket Socket) {
        
        voiceSocket = Socket;
        
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            voiceSocket = new DatagramSocket();
            voiceSocket = Socket;
        } catch (SocketException se) {
            System.err.println(se);
        } catch (IOException e) {
            System.err.println(e);
        }
        
    }

    /**
     * Component initialization
     */
    private void jbInit() throws Exception {
        //setIconImage(Toolkit.getDefaultToolkit().createImage(ClientInterfaceFrame.class.getResource("[Your Icon]")));
        contentPane = (JPanel) this.getContentPane();
        contentPane.setLayout(null);
        this.setSize(new Dimension(450, 550));
        this.setTitle("ClientInterface");
        jPanel1.setBorder(BorderFactory.createEtchedBorder());
        jPanel1.setBounds(new Rectangle(8, 8, 308, 65));
        jPanel1.setLayout(null);
        jPanel2.setBounds(new Rectangle(8, 80, 308, 106));
        jPanel2.setLayout(null);
        jPanel2.setBorder(BorderFactory.createEtchedBorder());
        
        userIdentityInput.setToolTipText("Enter your Username");
        userIdentityInput.setBounds(new Rectangle(10, 30, 288, 25));
        userIdentityInput.setText("1");
        friendIdentityInput.setEnabled(true);
        friendIdentityInput.setBounds(new Rectangle(9, 25, 288, 25));
        
        friendIdentityInput.setText("2");
        usernameLabel.setBounds(new Rectangle(10, 10, 177, 16));
        usernameLabel.setText("User Identity");
        mesageToLabel.setBounds(new Rectangle(9, 7, 177, 16));
        mesageToLabel.setText("Friend Identity");
        
        
        transferUserIdentityInput.setText("3");/////////////////////////////////////////////////////////////////////////////////////////////
        transferUserIdentityInput.setBounds(new Rectangle(10, 75, 288, 25));
        transferUserIdentityInput.setToolTipText("Enter Username Whom You Want to Transfer");
        transferUserIdentityInput.setEnabled(false);
        
        transferUsernameLabel.setText("Transfer User Identity");
        transferUsernameLabel.setBounds(new Rectangle(9, 55, 177, 16));
        transferUsernameLabel.setEnabled(true);///////////////////////////////////////////////////////////////////////////////////////
        
        sendRegButton.setBounds(new Rectangle(324, 33, 103, 29));
        sendRegButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                sendRegButtonAction(e);
            }
        });
        
        sendRegButton.setEnabled(true);
        sendRegButton.setToolTipText("Press To Join");
        sendRegButton.setText("Send Register");
        
        
        
        
        sendTransferButton.setBounds(new Rectangle(324, 145, 103, 29));/////////////////////////////////////////////////////////////////////
        sendTransferButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                sendTransferButtonAction(e);
            }
        });
        
        sendTransferButton.setEnabled(false);
        sendTransferButton.setToolTipText("Press To Transfer");
        sendTransferButton.setText("Transfer Call");////////////////////////////////////////////////////////////////////////////////////////
        
        
        
        busyTransferButton.setBounds(new Rectangle(324, 115, 103, 29));/////////////////////////////////////////////////////////////////////
        busyTransferButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                busyTransferButtonAction(e);
            }
        });
        
        busyTransferButton.setEnabled(false);
        busyTransferButton.setToolTipText("Press To send busy signal");
        busyTransferButton.setText("T Busy");////////////////////////////////////////////////////////////////////////////////////////
        
        
        answerTransferButton.setBounds(new Rectangle(324, 85, 103, 29));/////////////////////////////////////////////////////////////////////
        answerTransferButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                answerTransferButtonAction(e);
            }
        });
        
        answerTransferButton.setEnabled(false);
        answerTransferButton.setToolTipText("Press To Answer");
        answerTransferButton.setText("T Answer");////////////////////////////////////////////////////////////////////////////////////////
        
        
        callButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                answerButton.setEnabled(false);
                busyButton.setEnabled(false);
                callButton.setEnabled(false);
                byeButton.setEnabled(false);
                cancelButton.setEnabled(true);
                callButtonAction(e);
            }
        });
        callButton.setBounds(new Rectangle(322, 197, 103, 29));
        callButton.setText("Call");
        callButton.setToolTipText("Must Join Chat Before Sending Message");
        callButton.setEnabled(false);
        
        byeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                displayInGUI.append("You Ended the Call");
                
                answerButton.setEnabled(false);
                busyButton.setEnabled(false);
                callButton.setEnabled(true);
                byeButton.setEnabled(false);
                cancelButton.setEnabled(false);
                
                byeButtonAction(e);
            }
        });
        
        byeButton.setBounds(new Rectangle(322, 236, 103, 29));
        byeButton.setText("Bye");
        byeButton.setToolTipText("Must Join Chat Before Sending Message");
        byeButton.setEnabled(false);
        
        answerButton.setEnabled(false);
        answerButton.setToolTipText("Must Join Before Typing");
        answerButton.setText("Answer");
        answerButton.setBounds(new Rectangle(323, 274, 103, 29));
        answerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                answerButton.setEnabled(false);
                busyButton.setEnabled(false);
                callButton.setEnabled(false);
                byeButton.setEnabled(true);
                cancelButton.setEnabled(false);
                displayInGUI.append("You Answered the Call\n");
                answerButtonAction(e);
            }
        });
        
        busyButton.setEnabled(false);
        busyButton.setToolTipText("Must Join Before Typing");
        busyButton.setText("Busy");
        busyButton.setBounds(new Rectangle(323, 314, 103, 29));
        busyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                displayInGUI.append("You are busy Now\n");
                answerButton.setEnabled(false);
                busyButton.setEnabled(false);
                callButton.setEnabled(true);
                byeButton.setEnabled(false);
                cancelButton.setEnabled(false);
                busyButtonAction(e);
            }
        });
        
        cancelButton.setEnabled(false);
        cancelButton.setBounds(new Rectangle(323, 354, 102, 29));
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                displayInGUI.append("You cancled the Call\n");
                answerButton.setEnabled(false);
                busyButton.setEnabled(false);
                callButton.setEnabled(true);
                byeButton.setEnabled(false);
                cancelButton.setEnabled(false);
                cancelButtonAction(e);
            }
        });
        cancelButton.setToolTipText("Press To Leave");
        cancelButton.setText("Cancel");
        
        leaveButton.setBounds(new Rectangle(323, 394, 102, 29));
        leaveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                answerButton.setEnabled(false);
                busyButton.setEnabled(false);
                callButton.setEnabled(true);
                byeButton.setEnabled(false);
                cancelButton.setEnabled(false);
                callButton.setEnabled(false);
                sendRegButton.setEnabled(true);
                userIdentityInput.setEnabled(true);
                friendIdentityInput.setEditable(true);
                leaveButton_actionPerformed(e);
            }
        });
        leaveButton.setToolTipText("Press To Quit");
        leaveButton.setText("Leave");
        
        quitButton.setBounds(new Rectangle(323, 434, 102, 29));
        quitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                QuitButton_actionPerformed(e);
            }
        });
        quitButton.setToolTipText("Press To Quit");
        quitButton.setText("Quit");
        
        
        
        jScrollPane1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setBounds(new Rectangle(8, 200, 307, 222));
        displayInGUI.setBackground(Color.white);
        displayInGUI.setEditable(false);
        contentPane.add(jPanel1, null);
        jPanel1.add(userIdentityInput, null);
        jPanel1.add(usernameLabel, null);
        contentPane.add(jPanel2, null);
        jPanel2.add(friendIdentityInput, null);
        jPanel2.add(mesageToLabel, null);
        contentPane.add(answerButton, null);
        contentPane.add(busyButton, null);
        contentPane.add(leaveButton, null);
        contentPane.add(quitButton, null);
        contentPane.add(sendRegButton, null);
        
        contentPane.add(sendTransferButton, null);
        contentPane.add(busyTransferButton, null);
        contentPane.add(answerTransferButton, null);
        jPanel2.add(transferUserIdentityInput, null);
        jPanel2.add(transferUsernameLabel, null);
        
        contentPane.add(cancelButton, null);
        contentPane.add(callButton, null);
        contentPane.add(byeButton, null);
        contentPane.add(jScrollPane1, null);
        jScrollPane1.getViewport().add(displayInGUI, null);
    }

    /**
     * Overridden so we can exit when window is closed
     */
    protected void processWindowEvent(WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == WindowEvent.WINDOW_CLOSING) {
            System.exit(0);
        }
    }
    
    void sendRegButtonAction(ActionEvent e) {
        
        voicechatparser.VoiceChatParser parser = voicechatparser.VoiceChatParser.getInstance();
        
        packetType = voicechatparser.Constants.VOICE_REGISTER;
        GetUserInput();
        if (userIdentity > 0 && friendIdentity > 0) {
            String packetID = Constants.getRandromPacketId();
            //finalBytePacket = PacketProcessor.makeRegisterPacket(packetType, userIdentity, friendIdentity, packetID);
            finalBytePacket = parser.makeRegisterPacket(packetType, userIdentity, friendIdentity, packetID);
            sendRegisterPacket(finalBytePacket);
            packetType = voicechatparser.Constants.VOICE_ADDRESS_REQUEST;
            //finalBytePacket = PacketProcessor.makeAddressRequestPacket(packetType, packetID, userIdentity);
            finalBytePacket = parser.makeAddressRequestPacket(packetType, packetID, userIdentity);
            sendRegisterPacket(finalBytePacket);
            
//            packetID = voicechatparser.Constants.getRandromPacketId();
//            finalBytePacket = PacketProcessor.makePushPacket(voicechatparser.Constants.VOICE_REGISTER_PUSH, packetID, userIdentity, "Md. Anwar Hossain", friendIdentity, 3, "1318fec5e6ac2308de947def5a1cb510bc7fe4488688854c4b45fe043fe91af6");
//            sendRegisterPacket(finalBytePacket);

        }
    }
    
    
    void sendTransferButtonAction(ActionEvent e) {////////////////////////////////////////////////////////////////////////////////////////////////////
        if (transferUserIdentityInput.getText() != null) {
            transferUserIdentity = Long.parseLong(transferUserIdentityInput.getText().toString());
        }
        
        voicechatparser.VoiceChatParser parser = voicechatparser.VoiceChatParser.getInstance();
        
        GetUserInput();
        if(transferUserIdentity>0)
        {
            // 2->1
//            GetUserInput();
//            packetType = voicechatparser.Constants.TRANSFER_CALL.HOLD;
//            String packetID = voicechatparser.Constants.getRandromPacketId();
//            finalBytePacket = PacketProcessor.makeTransferPacket(packetType, userIdentity, friendIdentity, packetID,transferUserIdentity);
//            sendSignalingPacket(packetID + "_" + voicechatparser.Constants.TRANSFER_CALL.HOLD,finalBytePacket);
//            System.out.println("TRANSFER_CALL.HOLD sent from "+ userIdentity+" to "+ friendIdentity);
            
            
            //2->3
            GetUserInput();
            packetType = voicechatparser.Constants.TRANSFER_CALL.TRANSFER;
            Constants.TRANSFER_CALL_ID = Constants.getRandromPacketId();
            //finalBytePacket = PacketProcessor.makeTransferPacket(packetType, userIdentity, transferUserIdentity, voicechatparser.Constants.TRANSFER_CALL_ID,friendIdentity);
            finalBytePacket = parser.makeTransferPacket(packetType, userIdentity, transferUserIdentity, Constants.TRANSFER_CALL_ID, friendIdentity);
            sendSignalingPacket(Constants.TRANSFER_CALL_ID + "_" + voicechatparser.Constants.TRANSFER_CALL.TRANSFER,finalBytePacket);
            System.out.println("VOICE_TRANSFER send from "+ userIdentity+" to "+ transferUserIdentity);
            
            displayInGUI.append("\nTrying to transfer to " + transferUserIdentity +"\n");
            
            //ClientRecieve.displayInGUI.append(messageDTO.getFriendIdentity() + " is available.\n");
        }
    }
            
          
   
    void answerTransferButtonAction(ActionEvent e) {////////////////////////////////////////////////////////////////////////////////////////////////////
        voicechatparser.VoiceChatParser parser = voicechatparser.VoiceChatParser.getInstance();
        if (transferUserIdentityInput.getText() != null) {
            transferUserIdentity = Long.parseLong(transferUserIdentityInput.getText().toString());
        }
        GetUserInput();
        if(transferUserIdentity>0)
        {
            // 2->1
            GetUserInput();
            packetType = voicechatparser.Constants.TRANSFER_CALL.SUCCESS;
            //String packetID = voicechatparser.Constants.getRandromPacketId();
//            finalBytePacket = PacketProcessor.makeSignalingPacket(packetType, voicechatparser.Constants.TRANSFER_CALL_ID, userIdentity, friendIdentity);
            finalBytePacket = parser.makeSignalingPacket(packetType, Constants.TRANSFER_CALL_ID, userIdentity, friendIdentity);
            sendSignalingPacket(Constants.TRANSFER_CALL_ID + "_" + voicechatparser.Constants.TRANSFER_CALL.SUCCESS_CONFIRMATION,finalBytePacket);
            System.out.println("TRANSFER_CALL.SUCCESS sent from "+ userIdentity+" to "+ friendIdentity);
            

            GetUserInput();
            packetType = voicechatparser.Constants.TRANSFER_CALL.CONNECTED;
            //packetID = voicechatparser.Constants.getRandromPacketId();
//            finalBytePacket = PacketProcessor.makeSignalingPacket(packetType, voicechatparser.Constants.TRANSFER_CALL_ID, userIdentity, toTransferFriendIdentity);
            finalBytePacket = parser.makeSignalingPacket(packetType, Constants.TRANSFER_CALL_ID, userIdentity, toTransferFriendIdentity);
            sendSignalingPacket(Constants.TRANSFER_CALL_ID + "_" + voicechatparser.Constants.TRANSFER_CALL.CONNECTED_CONFIRMATION,finalBytePacket);
            System.out.println("TRANSFER_CALL.SUCCESS sent from "+ userIdentity+" to "+ toTransferFriendIdentity);
            
    
        }
    }
    
    
    void busyTransferButtonAction(ActionEvent e) {
        voicechatparser.VoiceChatParser parser = voicechatparser.VoiceChatParser.getInstance();
        GetUserInput();
        packetType = voicechatparser.Constants.TRANSFER_CALL.BUSY;
        //voicechatparser.Constants.CALL_ID = voicechatparser.Constants.getRandromPacketId();
        //finalBytePacket = PacketProcessor.makeSignalingPacket(packetType, voicechatparser.Constants.CALL_ID, userIdentity, friendIdentity);
        finalBytePacket = parser.makeSignalingPacket(packetType, Constants.CALL_ID, userIdentity, friendIdentity);
        System.out.println("busyTransfer signal send from "+ userIdentity+" to "+ friendIdentity);
        sendSignalingPacket(Constants.CALL_ID + "_" + voicechatparser.Constants.TRANSFER_CALL.BUSY_CONFIRMATION, finalBytePacket);
        
    }
    
            
    void callButtonAction(ActionEvent e) {
        voicechatparser.VoiceChatParser parser = voicechatparser.VoiceChatParser.getInstance();
        packetType = voicechatparser.Constants.CALL_STATE.CALLING;
        callButton.setEnabled(false);
        byeButton.setEnabled(false);
        answerButton.setEnabled(false);
        busyButton.setEnabled(false);
        cancelButton.setEnabled(true);
        GetUserInput();
        if (userIdentity > 0 && friendIdentity>0) {
            
            String packetID = Constants.getRandromPacketId();
//            finalBytePacket = PacketProcessor.makeAvailabilityPacket(voicechatparser.Constants.CALL_STATE.USER_AVAILABLE, packetID, friendIdentity);
            finalBytePacket = parser.makeAvailabilityPacket(voicechatparser.Constants.CALL_STATE.USER_AVAILABLE, packetID, friendIdentity);
             
            sendSignalingPacket(packetID, finalBytePacket);
            
            Constants.CALL_ID = Constants.getRandromPacketId();
//            finalBytePacket = PacketProcessor.makeSignalingPacket(packetType, voicechatparser.Constants.CALL_ID, userIdentity, friendIdentity);
            finalBytePacket = parser.makeSignalingPacket(packetType, Constants.CALL_ID, userIdentity, friendIdentity);
            
//            for(int i=0;i<finalBytePacket.length;i++)
//                System.out.print(finalBytePacket[i]+" ");
//           System.out.println();
           
            sendSignalingPacket(Constants.CALL_ID + "_" + voicechatparser.Constants.CALL_STATE.RINGING, finalBytePacket);
            
            packetID = Constants.getRandromPacketId();
//            finalBytePacket = PacketProcessor.makeRegisterPacket(voicechatparser.Constants.CALL_STATE.KEEPALIVE, userIdentity, friendIdentity, packetID);
            finalBytePacket = parser.makeRegisterPacket(voicechatparser.Constants.CALL_STATE.KEEPALIVE, userIdentity, friendIdentity, packetID);
            sendSignalingPacket(packetID, finalBytePacket);
            
        }
    }
    
    void byeButtonAction(ActionEvent e) {
        voicechatparser.VoiceChatParser parser = voicechatparser.VoiceChatParser.getInstance();
        packetType = voicechatparser.Constants.CALL_STATE.BYE;
        callButton.setEnabled(true);
        byeButton.setEnabled(false);
        answerButton.setEnabled(false);
        busyButton.setEnabled(false);
        cancelButton.setEnabled(false);
        if(!isTransferred)
            GetUserInput();
        if (userIdentity > 0) {
//            finalBytePacket = PacketProcessor.makeSignalingPacket(packetType, voicechatparser.Constants.CALL_ID, userIdentity, friendIdentity);
            finalBytePacket = parser.makeSignalingPacket(packetType, Constants.CALL_ID, userIdentity, friendIdentity);
            sendSignalingPacket(Constants.CALL_ID + "_" + voicechatparser.Constants.CALL_STATE.DISCONNECTED, finalBytePacket);
        }
    }
    
    void busyButtonAction(ActionEvent e) {
        voicechatparser.VoiceChatParser parser = voicechatparser.VoiceChatParser.getInstance();
        packetType = voicechatparser.Constants.CALL_STATE.BUSY;
        GetUserInput();
        callButton.setEnabled(true);
        byeButton.setEnabled(false);
        answerButton.setEnabled(false);
        busyButton.setEnabled(false);
        cancelButton.setEnabled(false);
//        finalBytePacket = PacketProcessor.makeSignalingPacket(packetType, voicechatparser.Constants.CALL_ID, userIdentity, friendIdentity);
        finalBytePacket = parser.makeSignalingPacket(packetType, Constants.CALL_ID, userIdentity, friendIdentity);
        sendSignalingPacket(Constants.CALL_ID + "_" + voicechatparser.Constants.CALL_STATE.DISCONNECTED, finalBytePacket);
    }
    
    void answerButtonAction(ActionEvent e) {
        voicechatparser.VoiceChatParser parser = voicechatparser.VoiceChatParser.getInstance();
        packetType = voicechatparser.Constants.CALL_STATE.ANSWER;
        GetUserInput();
        callButton.setEnabled(false);
        byeButton.setEnabled(true);
        answerButton.setEnabled(false);
        busyButton.setEnabled(false);
        cancelButton.setEnabled(false);
        //finalBytePacket = PacketProcessor.makeSignalingPacket(packetType, voicechatparser.Constants.CALL_ID, userIdentity, friendIdentity);
        finalBytePacket = parser.makeSignalingPacket(packetType, Constants.CALL_ID, userIdentity, friendIdentity);
        sendSignalingPacket(Constants.CALL_ID + "_" + voicechatparser.Constants.CALL_STATE.CONNECTED, finalBytePacket);
    }
    
    void cancelButtonAction(ActionEvent e) {
        voicechatparser.VoiceChatParser parser = voicechatparser.VoiceChatParser.getInstance();
        packetType = voicechatparser.Constants.CALL_STATE.CANCELED;
        GetUserInput();
        //finalBytePacket = PacketProcessor.makeSignalingPacket(packetType, voicechatparser.Constants.CALL_ID, userIdentity, friendIdentity);
        finalBytePacket = parser.makeSignalingPacket(packetType, Constants.CALL_ID, userIdentity, friendIdentity);
        sendSignalingPacket(Constants.CALL_ID + "_" + voicechatparser.Constants.CALL_STATE.DISCONNECTED, finalBytePacket);
        callButton.setEnabled(true);
        byeButton.setEnabled(false);
        answerButton.setEnabled(false);
        busyButton.setEnabled(false);
        cancelButton.setEnabled(false);
    }
    
    void QuitButton_actionPerformed(ActionEvent e) {
        voicechatparser.VoiceChatParser parser = voicechatparser.VoiceChatParser.getInstance();
        packetType = voicechatparser.Constants.VOICE_UNREGISTERED;
        GetUserInput();
        if (userIdentity > 0) {
            //finalBytePacket = PacketProcessor.makeUnRegisterPacket(packetType, userIdentity);
            finalBytePacket = parser.makeUnRegisterPacket(packetType, userIdentity);
            sendRegisterPacket(finalBytePacket);
        }
        //Exit Interface
        System.exit(0);
    }
    
    void leaveButton_actionPerformed(ActionEvent e) {
        voicechatparser.VoiceChatParser parser = voicechatparser.VoiceChatParser.getInstance();
        packetType = voicechatparser.Constants.VOICE_UNREGISTERED;
        GetUserInput();
        if (userIdentity > 0) {
            //finalBytePacket = PacketProcessor.makeUnRegisterPacket(packetType, userIdentity);
            finalBytePacket = parser.makeUnRegisterPacket(packetType, userIdentity);
            sendRegisterPacket(finalBytePacket);
        }
    }
    
    public void GetUserInput() {
        if (userIdentityInput.getText() != null) {
            userIdentity = Long.parseLong(userIdentityInput.getText().toString());
        }
        if (friendIdentityInput.getText() != null) {
            friendIdentity = Long.parseLong(friendIdentityInput.getText().toString());
        }
    }
    
    public void sendRegisterPacket(byte[] sendingBytePacket) {
        try {
            finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, InetAddress.getByName(Constants.VOICE_SERVER_IP), Constants.VOICE_REGISTER_PORT);
            voiceSocket.send(finalPacket);
            System.out.println("Packet: " + new String(sendingBytePacket));
        } catch (UnknownHostException e) {
            System.err.println(e);
        } catch (IOException e) {
            System.err.println(e);
        }
    }
    
    public void sendSignalingPacket(String packetID, byte[] sendingBytePacket) {
        try {
            System.out.println(new String(sendingBytePacket).trim());
            finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, InetAddress.getByName(Constants.VOICE_SERVER_IP), Constants.VOICE_BINDING_PORT);
            voiceSocket.send(finalPacket);
            
            ResendPacketDTO resendPacketDTO = new ResendPacketDTO();
            resendPacketDTO.setUdpSocket(voiceSocket);
            resendPacketDTO.setPacket(finalPacket);
            resendPacketDTO.setNumberOfResend(1);
            Constants.MESSAGE_RESEND_MAP.put(packetID, resendPacketDTO);
            
        } catch (UnknownHostException e) {
            System.err.println(e);
        } catch (IOException e) {
            System.err.println(e);
        }
    }
    
    
    public void sendKeepAlivePacket(String packetID, final byte[] sendingBytePacket) {
        Thread t = new Thread(){
            public void run()
            {
                while(ClientInterface.isKeepAlive)
                {
                    try {
                        Thread.sleep(1000);
                        System.out.println(new String(sendingBytePacket).trim());
                        finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, InetAddress.getByName(Constants.VOICE_SERVER_IP), Constants.VOICE_BINDING_PORT);
                        voiceSocket.send(finalPacket);

            //            ResendPacketDTO resendPacketDTO = new ResendPacketDTO();
            //            resendPacketDTO.setUdpSocket(voiceSocket);
            //            resendPacketDTO.setPacket(finalPacket);
            //            resendPacketDTO.setNumberOfResend(1);
            //            voicechatparser.Constants.MESSAGE_RESEND_MAP.put(packetID, resendPacketDTO);

                    } catch (UnknownHostException e) {
                        System.err.println(e);
                    } catch (IOException e) {
                        System.err.println(e);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ClientInterfaceFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        t.start();
    }
    
    public JTextArea GetTextArea() {
        return displayInGUI;
    }
}