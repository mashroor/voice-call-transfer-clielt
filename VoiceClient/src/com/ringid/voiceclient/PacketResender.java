/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.voiceclient;

import com.ringid.utils.Constants;
import java.io.IOException;
import java.util.Set;

/**
 *
 * @author Anwar
 */
public class PacketResender extends Thread {

    private boolean running = false;
    long PROCESSING_INTERVAL = 500L;

    public PacketResender() {
        running = true;
    }

    @Override
    public void run() {
        while (running) {
            Set<String> keySet = Constants.MESSAGE_RESEND_MAP.keySet();
            for (String key : keySet) {
                ResendPacketDTO resendPacket = Constants.MESSAGE_RESEND_MAP.get(key);
                if (resendPacket != null && resendPacket.getNumberOfResend() < Constants.NUMBER_OF_RESEND) {
                    try {
                        resendPacket.getUdpSocket().send(resendPacket.getPacket());
                    } catch (IOException ex) {
                    }
                    resendPacket.setNumberOfResend(resendPacket.getNumberOfResend() + 1);
                } else {
                    Constants.MESSAGE_RESEND_MAP.remove(key);
                }
            }
            try {
                if (running) {
                    Thread.sleep(PROCESSING_INTERVAL);
                }
            } catch (Exception ex) {
            }
        }
    }

    public void stopService() {
        running = false;
    }
}
