package com.ringid.voiceclient;

import java.awt.*;
import java.io.*;
import java.net.*;
import javax.swing.UIManager;


/**
 *
 * @author Anwar
 */
public class ClientInterface {

    boolean packFrame = false;
    DatagramSocket ChatSocket;
    static boolean isKeepAlive = false;

    /**
     * Construct the application
     */
    public ClientInterface() {

        try {
            //create the master thread for the new client interface
            ChatSocket = new DatagramSocket();
        } catch (SocketException se) {
            System.err.println(se);
        } catch (IOException e) {
            System.err.println(e);
        }

//create the new UI frame and recieving thread.
//Pass each a reference of the master thread.
//pass the thread the frame text area and the entire frame.
//Didn't need to pass the frame.textare if already passing the whole frame,
//but had already written it this way and not worth changing the whole thing.

        ClientInterfaceFrame frame = new ClientInterfaceFrame(ChatSocket);
        Thread r = new ClientRecieve(frame.GetTextArea(), ChatSocket, frame);
        r.start();

        //Validate frames that have preset sizes
        //Pack frames that have useful preferred size info, e.g. from their layout
        if (packFrame) {
            frame.pack();
        } else {
            frame.validate();
        }
        //Center the window
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = frame.getSize();
        if (frameSize.height > screenSize.height) {
            frameSize.height = screenSize.height;
        }
        if (frameSize.width > screenSize.width) {
            frameSize.width = screenSize.width;
        }
        frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
        frame.setVisible(true);
    }

    /**
     * Main method
     */
    public static void main(String[] args) {
        
//        VoiceChatParser parser = VoiceChatParser.getInstance();
//        System.out.println(parser.version());
//        
//        byte[] arr = parser.makeTransferPacket(28, 1, 2, "0123456789" , 3);
//        
//        for(int i=0;i<arr.length;i++)
//            System.out.print(arr[i]+" ");
//        System.out.println();
//        
//        arr = parser.makeSignalingPacket(6, "0123456789", 9, 10);
//        
//        for(int i=0;i<arr.length;i++)
//            System.out.print(arr[i]+" ");
//        System.out.println();
        
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        new ClientInterface();
    }
}