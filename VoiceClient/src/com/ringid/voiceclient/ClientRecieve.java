package com.ringid.voiceclient;

import com.ringid.media.AudioStreamPlayer;
import com.ringid.media.AudioStreamRecorder;
import com.ringid.utils.Constants;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author Anwar
 */
public class ClientRecieve extends Thread {

    static JTextArea displayInGUI = new JTextArea();
    static boolean isInCall = false;/////////////////////////////////////////////////////////////////////////////////////////////////////
    static final int BUFFER_SIZE = 10007;           //Sets Max Packet Size
    private DatagramSocket chatDatagramSocket = null;     //Socket to Communicate with
    int packetType;
    String receiveStrings;
    long userIdentity;
    long friendIdentity;
    String message;
    String finalMessage;
    ClientInterfaceFrame clientInterfaceFrame;
    AudioStreamPlayer audioStreamPlayer;
    AudioStreamRecorder audioStreamRecorder;
    //static AudioStreamPlayer audioStreamPlayer;
    //static AudioStreamRecorder audioStreamRecorder;

    public ClientRecieve(JTextArea TextMessageDisplay, DatagramSocket Socket, ClientInterfaceFrame PassedGUI) {
        chatDatagramSocket = Socket;
        displayInGUI = TextMessageDisplay;
        clientInterfaceFrame = PassedGUI;
    }

    public void run() {
        try {
            do {
                //Block until a Datagram appears
                byte[] buf = new byte[BUFFER_SIZE];
                DatagramPacket receivedPacket = new DatagramPacket(buf, buf.length);
                System.out.println("we're recieving");
                chatDatagramSocket.receive(receivedPacket);
                buf = receivedPacket.getData();
                ProccesPacket(buf, receivedPacket.getLength());
            } while (true);

        } catch (IOException e) {
            System.err.println("Communication error" + e);
        }

    }

    public void ProccesPacket(byte[] receivedBuffer, int length) throws UnknownHostException {
        
        voicechatparser.VoiceChatParser parser = voicechatparser.VoiceChatParser.getInstance();
        //byte[] add;
        InetAddress inetAddress;
        
        int totalRead = 0;
        String packetID;
        packetType = receivedBuffer[0];
        totalRead++;
        voicechatparser.MessageDTO messageDTO = null;
        byte[] confirmationByte;
        switch (packetType) {
            case voicechatparser.Constants.VOICE_MEDIA:
                
//                System.out.print("{"+receivedBuffer[0]);
//                for(int i=1;i<length;i++)
//                    System.out.print(","+receivedBuffer[i]);
//                System.out.println("}");
                
                messageDTO = parser.parse(receivedBuffer,length);
                
                if (audioStreamPlayer != null) {
                    //audioStreamPlayer.onReceivedMessage(receivedBuffer, length);
                    audioStreamPlayer.onReceivedMessage(messageDTO.getVoiceData(), length);
                    System.out.println("Length: " + length);
                }
                break;
            case voicechatparser.Constants.CALL_STATE.CALLING:
                //messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                displayInGUI.append(messageDTO.getUserIdentity()+ " is Calling...\n");
                Constants.CALL_ID = messageDTO.getPacketID();

                //confirmationByte = PacketProcessor.makeSignalingPacket(voicechatparser.Constants.CALL_STATE.RINGING, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity());
                confirmationByte = parser.makeSignalingPacket(voicechatparser.Constants.CALL_STATE.RINGING, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getUserIdentity());
                sendSignalingPacket(confirmationByte);

                clientInterfaceFrame.friendIdentityInput.setText("" + messageDTO.getUserIdentity());
                clientInterfaceFrame.callButton.setEnabled(false);
                clientInterfaceFrame.byeButton.setEnabled(false);
                clientInterfaceFrame.answerButton.setEnabled(true);
                clientInterfaceFrame.busyButton.setEnabled(true);
                clientInterfaceFrame.cancelButton.setEnabled(false);

                break;
            case voicechatparser.Constants.CALL_STATE.RINGING:
                //messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                Constants.MESSAGE_RESEND_MAP.remove(messageDTO.getPacketID() + "_" + voicechatparser.Constants.CALL_STATE.RINGING);
                displayInGUI.append(messageDTO.getUserIdentity()+ " is ringing...\n");
                //Need to start Player for playing ringBack Tone
                clientInterfaceFrame.callButton.setEnabled(false);
                clientInterfaceFrame.byeButton.setEnabled(false);
                clientInterfaceFrame.answerButton.setEnabled(false);
                clientInterfaceFrame.busyButton.setEnabled(false);
                clientInterfaceFrame.cancelButton.setEnabled(true);
                break;

            case voicechatparser.Constants.CALL_STATE.ANSWER:
//                messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                displayInGUI.append(messageDTO.getUserIdentity()+ " has Answered your call...\n");
                isInCall = true;
                //Caller END Need to Start Audio Recorder + Player
                //confirmationByte = PacketProcessor.makeSignalingPacket(voicechatparser.Constants.CALL_STATE.CONNECTED, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity());
                confirmationByte = parser.makeSignalingPacket(voicechatparser.Constants.CALL_STATE.CONNECTED, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getUserIdentity());
                sendSignalingPacket(confirmationByte);
                clientInterfaceFrame.callButton.setEnabled(false);
                clientInterfaceFrame.byeButton.setEnabled(true);
                clientInterfaceFrame.answerButton.setEnabled(false);
                clientInterfaceFrame.busyButton.setEnabled(false);
                clientInterfaceFrame.cancelButton.setEnabled(false);
                clientInterfaceFrame.sendTransferButton.setEnabled(true);
                clientInterfaceFrame.transferUserIdentityInput.setEnabled(true);
//                if (clientInterfaceFrame.audioApp == null) {
//                    FlowSpec.Direction dir = FlowSpec.FULL_DUPLEX;
//                    FlowSpec flow_spec = new FlowSpec(voicechatparser.Constants.VOICE_SERVER_IP, voicechatparser.Constants.VOICE_BINDING_PORT, dir);
//                    String audio_file_in = null;
//                    String audio_file_out = null;
//                    int random_early_drop_rate = 20;
//                    clientInterfaceFrame.audioApp = new AudioApp(flow_spec, audio_file_in, audio_file_out, false, true, random_early_drop_rate, false, chatDatagramSocket);
//                    clientInterfaceFrame.audioApp.startApp();
//                }
                if (audioStreamRecorder == null) {
                    audioStreamRecorder = new AudioStreamRecorder(clientInterfaceFrame.voiceSocket);
                    //displayInGUI.append("\naudio recorder Started\n");
                    System.err.println("audio recorder Started");
                }
                if (audioStreamPlayer == null) {
                    audioStreamPlayer = new AudioStreamPlayer();
                    //displayInGUI.append("\naudio player Started\n");
                    System.err.println("audio player Started");
                }
                //displayInGUI.append("\nANSWER\n");
                break;
            case voicechatparser.Constants.CALL_STATE.CONNECTED:
                //Callee End Need to Start Audio Recorder + Player

                //messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                
                displayInGUI.append(messageDTO.getUserIdentity()+ " has Connected with your call...\n");

//                messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                Constants.MESSAGE_RESEND_MAP.remove(messageDTO.getPacketID() + "_" + voicechatparser.Constants.CALL_STATE.CONNECTED);
                clientInterfaceFrame.callButton.setEnabled(false);
                clientInterfaceFrame.byeButton.setEnabled(true);
                clientInterfaceFrame.answerButton.setEnabled(false);
                clientInterfaceFrame.busyButton.setEnabled(false);
                clientInterfaceFrame.cancelButton.setEnabled(false);
                clientInterfaceFrame.sendTransferButton.setEnabled(true);
                clientInterfaceFrame.transferUserIdentityInput.setEnabled(true);

                if (audioStreamRecorder == null) {
                    audioStreamRecorder = new AudioStreamRecorder(clientInterfaceFrame.voiceSocket);
                    //displayInGUI.append("\naudio recorder Started\n");
                    System.err.println("audio recorder Started");
//                    recorder.start();
                }
                if (audioStreamPlayer == null) {
                    audioStreamPlayer = new AudioStreamPlayer();
                    //displayInGUI.append("\naudio player Started\n");                    
                    System.err.println("audio player Started");
//                    player.start();
                }
                //displayInGUI.append("\nCONNECTED\n");
                break;
            case voicechatparser.Constants.CALL_STATE.BUSY:
                //Need to Stop Player + Receiver
                //messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                displayInGUI.append(messageDTO.getUserIdentity()+ " is Disconnected. \n");
//                confirmationByte = PacketProcessor.makeSignalingPacket(voicechatparser.Constants.CALL_STATE.DISCONNECTED, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity());
                confirmationByte = parser.makeSignalingPacket(voicechatparser.Constants.CALL_STATE.DISCONNECTED, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getUserIdentity());
                sendSignalingPacket(confirmationByte);
                clientInterfaceFrame.callButton.setEnabled(true);
                clientInterfaceFrame.byeButton.setEnabled(false);
                clientInterfaceFrame.answerButton.setEnabled(false);
                clientInterfaceFrame.busyButton.setEnabled(false);
                clientInterfaceFrame.cancelButton.setEnabled(false);
//                if (clientInterfaceFrame.audioApp != null) {
//                    clientInterfaceFrame.audioApp.stopApp();
//                    clientInterfaceFrame.audioApp = null;
//                }

                if (audioStreamRecorder != null) {
                    audioStreamRecorder.stopRecorder();
                    audioStreamRecorder = null;
                    //displayInGUI.append("\naudio recorder Stopped\n");
                    System.err.println("audio recorder Stopped");
                }
                if (audioStreamPlayer != null) {
                    audioStreamPlayer.stopPlayer();
                    audioStreamPlayer = null;
                    //displayInGUI.append("\naudio player Stopped\n");
                    System.err.println("audio player Stopped");
                }
                //displayInGUI.append("\nBUSY\n");
                break;
            case voicechatparser.Constants.CALL_STATE.CANCELED:
                //Need to Stop Player + Receiver
//                messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                
               
                messageDTO = parser.parse(receivedBuffer,0);
                displayInGUI.append(messageDTO.getUserIdentity()+ " has missed call you.\n");
                //confirmationByte = PacketProcessor.makeSignalingPacket(voicechatparser.Constants.CALL_STATE.DISCONNECTED, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity());
                confirmationByte = parser.makeSignalingPacket(voicechatparser.Constants.CALL_STATE.DISCONNECTED, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getUserIdentity());
                sendSignalingPacket(confirmationByte);

                clientInterfaceFrame.callButton.setEnabled(true);
                clientInterfaceFrame.byeButton.setEnabled(false);
                clientInterfaceFrame.answerButton.setEnabled(false);
                clientInterfaceFrame.busyButton.setEnabled(false);
                clientInterfaceFrame.cancelButton.setEnabled(false);

                if (audioStreamRecorder != null) {
                    audioStreamRecorder.stopRecorder();
                    audioStreamRecorder = null;
                    //displayInGUI.append("\naudio recorder Stopped\n");
                    System.err.println("audio recorder Stopped");
                }
                if (audioStreamPlayer != null) {
                    audioStreamPlayer.stopPlayer();
                    audioStreamPlayer = null;
                    //displayInGUI.append("\naudio player Stopped\n");
                    System.err.println("audio player Stopped");
                }
                //displayInGUI.append("\nCANCELED\n");
                break;
            case voicechatparser.Constants.CALL_STATE.BYE:
//                messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                isInCall = false;
                displayInGUI.append(messageDTO.getUserIdentity()+ " has ENDED the Call.\n");
                //confirmationByte = PacketProcessor.makeSignalingPacket(voicechatparser.Constants.CALL_STATE.DISCONNECTED, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity());
                confirmationByte = parser.makeSignalingPacket(voicechatparser.Constants.CALL_STATE.DISCONNECTED, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getUserIdentity());
                sendSignalingPacket(confirmationByte);
                clientInterfaceFrame.callButton.setEnabled(true);
                clientInterfaceFrame.byeButton.setEnabled(false);
                clientInterfaceFrame.answerButton.setEnabled(false);
                clientInterfaceFrame.busyButton.setEnabled(false);
                clientInterfaceFrame.cancelButton.setEnabled(false);

                if (audioStreamRecorder != null) {
                    audioStreamRecorder.stopRecorder();
                    audioStreamRecorder = null;
                    //displayInGUI.append("\naudio recorder Stopped\n");
                    System.err.println("audio recorder Stopped");
                }
                if (audioStreamPlayer != null) {
                    audioStreamPlayer.stopPlayer();
                    audioStreamPlayer = null;
                    //displayInGUI.append("\naudio player Stopped\n");
                    System.err.println("audio player Stopped");
                }
                //displayInGUI.append("\nBYE\n");

                break;
            case voicechatparser.Constants.CALL_STATE.DISCONNECTED:
//                messageDTO = PacketProcessor.getIncallPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                Constants.MESSAGE_RESEND_MAP.remove(messageDTO.getPacketID() + "_" + voicechatparser.Constants.CALL_STATE.DISCONNECTED);
                if (audioStreamRecorder != null) {
                    audioStreamRecorder.stopRecorder();
                    audioStreamRecorder = null;
                    //displayInGUI.append("\naudio recorder Stopped\n");
                    System.err.println("audio recorder Stopped");
                }
                if (audioStreamPlayer != null) {
                    audioStreamPlayer.stopPlayer();
                    audioStreamPlayer = null;
                    //displayInGUI.append("\naudio player Stopped\n");
                    System.err.println("audio player Stopped");
                }
                //displayInGUI.append("\nDISCONNECTED\n");
                break;
            case voicechatparser.Constants.CALL_STATE.IN_CALL:
                
                System.out.println("IN_CALL : 7");
                System.out.print("{"+receivedBuffer[0]);
                for(int i=1;i<length;i++)
                    System.out.print(","+receivedBuffer[i]);
                System.out.println("}");
                
                //messageDTO = PacketProcessor.getIncallPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                Constants.MESSAGE_RESEND_MAP.remove(messageDTO.getPacketID());
                displayInGUI.append(messageDTO.getFriendIdentity() + " is in another call Plz wait.\n");
                break;
            case voicechatparser.Constants.CALL_STATE.USER_AVAILABLE:
                
                System.out.println("USER_AVAILABLE : ");
                for(int i=0;i<length;i++)
                    System.out.print(receivedBuffer[i]+" ");
                System.out.println();
                
                //messageDTO = PacketProcessor.getUserAvailabilityPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                Constants.MESSAGE_RESEND_MAP.remove(messageDTO.getPacketID());
                displayInGUI.append(messageDTO.getFriendIdentity()+ " is available.\n");
                break;
            case voicechatparser.Constants.CALL_STATE.USER_NOT_AVAILABLE:
                messageDTO = parser.parse(receivedBuffer,0);
                
                System.out.println("USER_NOT_AVAILABLE : 17");
                System.out.print("{"+receivedBuffer[0]);
                for(int i=1;i<length;i++)
                    System.out.print(","+receivedBuffer[i]);
                System.out.println("}");
                
//                messageDTO = PacketProcessor.getUserAvailabilityPacket(receivedBuffer);
                Constants.MESSAGE_RESEND_MAP.remove(messageDTO.getPacketID());
                displayInGUI.append(messageDTO.getFriendIdentity()+ " is not available.\n");
                break;
            case voicechatparser.Constants.VOICE_REGISTER_CONFIRMATION:
                //messageDTO = PacketProcessor.getRegisterConfirmationPacket(receivedBuffer);
                
                System.out.println("VOICE REG CONF");
                
                for(int i=0;i<length;i++)
                {
                    System.out.print(receivedBuffer[i]+" ");
                }
                            
                
                messageDTO = parser.parse(receivedBuffer,0);
                Constants.VOICE_BINDING_PORT = messageDTO.getVoiceBindingPort();
                packetID = messageDTO.getPacketID();
                
                //add = messageDTO.getMapAddress().getBytes();
                
//                inetAddress = null;
//        
//                try {
//                    //inetAddress = InetAddress.getByAddress(add);
//                    inetAddress = InetAddress.getByName(messageDTO.getMapAddress());
//                    messageDTO.setInetAddress(inetAddress);
//                } catch (UnknownHostException ex) {
//                }
                
//                confirmationByte = PacketProcessor.makeRegisterPacket(voicechatparser.Constants.CALL_STATE.KEEPALIVE, clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity(), packetID);
                confirmationByte = parser.makeRegisterPacket(voicechatparser.Constants.CALL_STATE.KEEPALIVE, clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity(), packetID);
                sendSignalingPacket(confirmationByte);
                //sendKeepAlivePacket(confirmationByte);//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                
//                confirmationByte = PacketProcessor.makeAddressRequestPacket(voicechatparser.Constants.VOICE_ADDRESS_REQUEST, packetID, clientInterfaceFrame.userIdentity);
                confirmationByte = parser.makeAddressRequestPacket(voicechatparser.Constants.VOICE_ADDRESS_REQUEST, packetID, clientInterfaceFrame.userIdentity);
                sendSignalingPacket(confirmationByte);
                break;

            case voicechatparser.Constants.VOICE_REGISTER_PUSH_CONFIRMATION:
                
                System.out.println("VOICE_REGISTER_PUSH_CONFIRMATION : 21");
                System.out.print("{"+receivedBuffer[0]);
                for(int i=1;i<length;i++)
                    System.out.print(","+receivedBuffer[i]);
                System.out.println("}");
                
                //messageDTO = PacketProcessor.getPushConfirmationPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                System.out.println("PUSH Confirmation: " + new String(receivedBuffer));
                break;
            case voicechatparser.Constants.VOICE_ADDRESS:
                
                //messageDTO = PacketProcessor.getAddressPacket(receivedBuffer);
                System.out.println("VOICE_ADDRESS packet");
                for(int i=0 ; i<length;i++)
                {
                    System.out.print(receivedBuffer[i]+" ");
                }
                
                 System.out.println();
                
                messageDTO = parser.parse(receivedBuffer,0);
                //System.out.println("Address Packet: " + messageDTO.getInetAddress().getHostAddress() + ":" + messageDTO.getMapPort());
                
               // add = messageDTO.getMapAddress().getBytes();
                
//                inetAddress = null;
//        
//                try {
//                    //inetAddress = InetAddress.getByAddress(add);
//                    inetAddress = InetAddress.getByName(messageDTO.getMapAddress());
//                    messageDTO.setInetAddress(inetAddress);
//                } catch (UnknownHostException ex) {
//                }

                System.out.println("Address Packet: " + messageDTO.getMapAddress() + ":" + messageDTO.getMapPort());
                
                break;
                
            case voicechatparser.Constants.TRANSFER_CALL.HOLD:
                //messageDTO = PacketProcessor.getTransferSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                displayInGUI.append("\n"+clientInterfaceFrame.userIdentity+" <-> "+ messageDTO.getUserIdentity()+" Call On HOLD\n"+messageDTO.getTransferedUserIdentity()+" is Ringing..\n");
                System.out.println("hold packet received by active caller "+ clientInterfaceFrame.userIdentity +" "+ messageDTO.getUserIdentity()+" "+messageDTO.getTransferedUserIdentity());
    
//                confirmationByte = PacketProcessor.makeSignalingPacket(voicechatparser.Constants.TRANSFER_CALL.HOLD_CONFIRMATION, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity());
                confirmationByte = parser.makeSignalingPacket(voicechatparser.Constants.TRANSFER_CALL.HOLD_CONFIRMATION, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getUserIdentity());
                sendSignalingPacket(confirmationByte);
                //displayInGUI.append("\nTRANSFER_CALL.HOLD_CONFIRMATION sent\n");
                
                if (audioStreamRecorder != null) {
                    audioStreamRecorder.stopRecorder();
                    audioStreamRecorder = null;
                }
                if (audioStreamPlayer != null) {
                    audioStreamPlayer.stopPlayer();
                    audioStreamPlayer = null;
                }
                
                //displayInGUI.append("\nkeep alive sending"+clientInterfaceFrame.userIdentity+"->"+messageDTO.getFriendIdentity()+"\n");
                packetID = messageDTO.getPacketID();
                ClientInterface.isKeepAlive = true;
//                confirmationByte = PacketProcessor.makeRegisterPacket(voicechatparser.Constants.CALL_STATE.KEEPALIVE, clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity(), packetID);
                confirmationByte = parser.makeRegisterPacket(voicechatparser.Constants.CALL_STATE.KEEPALIVE, clientInterfaceFrame.userIdentity, messageDTO.getUserIdentity(), packetID);
                sendSignalingPacket(confirmationByte);
                
                break;
                
                
            case voicechatparser.Constants.TRANSFER_CALL.TRANSFER://///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //messageDTO = PacketProcessor.getTransferSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                clientInterfaceFrame.toTransferFriendIdentity = messageDTO.getTransferedUserIdentity();
                clientInterfaceFrame.busyTransferButton.setEnabled(true);
                clientInterfaceFrame.answerTransferButton.setEnabled(true);
                displayInGUI.append("\nIncoming transfered call from " + messageDTO.getTransferedUserIdentity()+"...\nTransfered by "+messageDTO.getUserIdentity()+"...");
                System.out.println("hold packet received by transfered caller "+ clientInterfaceFrame.userIdentity +" "+ messageDTO.getUserIdentity()+" "+messageDTO.getTransferedUserIdentity());
               
//                confirmationByte = PacketProcessor.makeSignalingPacket(voicechatparser.Constants.TRANSFER_CALL.TRANSFER_CONFIRMATION, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity());
                confirmationByte = parser.makeSignalingPacket(voicechatparser.Constants.TRANSFER_CALL.TRANSFER_CONFIRMATION, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getUserIdentity());
                sendSignalingPacket(confirmationByte);
                break;
                
            
            case voicechatparser.Constants.TRANSFER_CALL.HOLD_CONFIRMATION:
                  //messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                  messageDTO = parser.parse(receivedBuffer,0);
                  Constants.MESSAGE_RESEND_MAP.remove(messageDTO.getPacketID() + "_" + voicechatparser.Constants.TRANSFER_CALL.HOLD);
                  //displayInGUI.append("\nHOLD_CONFIRMATION received");
                  //displayInGUI.append("\nHOLD_CONFIRMATION received\n");
                  clientInterfaceFrame.sendTransferButton.setEnabled(false);

                  //displayInGUI.append("\nHOLD_CONFIRMATION received\n");
                  System.out.println("\nHOLD_CONFIRMATION received");
                  
                  if (audioStreamRecorder != null) {
                      audioStreamRecorder.stopRecorder();
                      audioStreamRecorder = null;
                  }
                  if (audioStreamPlayer != null) {
                      audioStreamPlayer.stopPlayer();
                      audioStreamPlayer = null;
                  }
                
                    //displayInGUI.append("\nkeep alive sending"+clientInterfaceFrame.userIdentity+"->"+messageDTO.getFriendIdentity()+"\n");
                    packetID = messageDTO.getPacketID();
                    ClientInterface.isKeepAlive = true;
//                    confirmationByte = PacketProcessor.makeRegisterPacket(voicechatparser.Constants.CALL_STATE.KEEPALIVE, clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity(), packetID);
                    confirmationByte = parser.makeRegisterPacket(voicechatparser.Constants.CALL_STATE.KEEPALIVE, clientInterfaceFrame.userIdentity, messageDTO.getUserIdentity(), packetID);
                    sendSignalingPacket(confirmationByte);
                
                     break;
                  
            case voicechatparser.Constants.TRANSFER_CALL.TRANSFER_CONFIRMATION:
//                messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                Constants.MESSAGE_RESEND_MAP.remove(messageDTO.getPacketID() + "_" + voicechatparser.Constants.TRANSFER_CALL.TRANSFER);
                
                //displayInGUI.append("\nTRANSFER_CONFIRMATION received");
                System.out.println("TRANSFER_CONFIRMATION received");
                  
                  // 2->1
            
                packetType = voicechatparser.Constants.TRANSFER_CALL.HOLD;
                packetID = Constants.getRandromPacketId();
//                confirmationByte = PacketProcessor.makeTransferPacket(packetType, clientInterfaceFrame.userIdentity, clientInterfaceFrame.friendIdentity, packetID,messageDTO.getFriendIdentity());
                confirmationByte = parser.makeTransferPacket(packetType, clientInterfaceFrame.userIdentity, clientInterfaceFrame.friendIdentity, packetID,messageDTO.getUserIdentity());
                sendSignalingPacket(confirmationByte);
                System.out.println("TRANSFER_CALL.HOLD sent from "+ clientInterfaceFrame.userIdentity+" to "+ clientInterfaceFrame.friendIdentity);
                //displayInGUI.append("\nTRANSFER_CALL.HOLD sent from "+ clientInterfaceFrame.userIdentity+" to "+ clientInterfaceFrame.friendIdentity+"\n");
                  
                break;
                  
                
            case voicechatparser.Constants.TRANSFER_CALL.BUSY:
//                  messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                  messageDTO = parser.parse(receivedBuffer,0);
                  displayInGUI.append("\n"+messageDTO.getUserIdentity()+" is busy");
                  System.out.println("TRANSFER_CALL.BUSY received");
//                  confirmationByte = PacketProcessor.makeSignalingPacket(voicechatparser.Constants.TRANSFER_CALL.BUSY_CONFIRMATION, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity());
                  confirmationByte = parser.makeSignalingPacket(voicechatparser.Constants.TRANSFER_CALL.BUSY_CONFIRMATION, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity());
                  sendSignalingPacket(confirmationByte);
                 // displayInGUI.append("\nvoicechatparser.Constants.TRANSFER_CALL.BUSY_CONFIRMATION "+clientInterfaceFrame.userIdentity+"->"+messageDTO.getFriendIdentity());
                  System.out.println("voicechatparser.Constants.TRANSFER_CALL.BUSY_CONFIRMATION "+clientInterfaceFrame.userIdentity+"->"+messageDTO.getUserIdentity());
                  
//                  confirmationByte = PacketProcessor.makeSignalingPacket(voicechatparser.Constants.TRANSFER_CALL.UNHOLD, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity , clientInterfaceFrame.friendIdentity);
                  confirmationByte = parser.makeSignalingPacket(voicechatparser.Constants.TRANSFER_CALL.UNHOLD, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity , clientInterfaceFrame.friendIdentity);
                  sendSignalingPacket(confirmationByte);
                  //displayInGUI.append("\nvoicechatparser.Constants.TRANSFER_CALL.UNHOLD "+clientInterfaceFrame.userIdentity+"->"+clientInterfaceFrame.friendIdentity);
                  System.out.println("voicechatparser.Constants.TRANSFER_CALL.UNHOLD "+clientInterfaceFrame.userIdentity+"->"+clientInterfaceFrame.friendIdentity);
                  
                  clientInterfaceFrame.sendTransferButton.setEnabled(true);
                  
                  ClientInterface.isKeepAlive = false;
                          
                  if (audioStreamRecorder == null) {
                      audioStreamRecorder = new AudioStreamRecorder(clientInterfaceFrame.voiceSocket);
//                    recorder.start();
                  }
                  if (audioStreamPlayer == null) {
                      audioStreamPlayer = new AudioStreamPlayer();
    //                player.start();
                  }
                  
                  //displayInGUI.append("\naudio started\n");
                  
                  break;
                  
            case voicechatparser.Constants.TRANSFER_CALL.BUSY_CONFIRMATION:
//                messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                Constants.MESSAGE_RESEND_MAP.remove(messageDTO.getPacketID() + "_" + voicechatparser.Constants.TRANSFER_CALL.CONNECTED_CONFIRMATION);
                clientInterfaceFrame.busyTransferButton.setEnabled(false);
                clientInterfaceFrame.answerTransferButton.setEnabled(false);
                //displayInGUI.append("\nTRANSFER_CALL.BUSY_CONFIRMATION received");
                System.out.println("TRANSFER_CALL.BUSY_CONFIRMATION received");
                break;
                  
            case voicechatparser.Constants.TRANSFER_CALL.UNHOLD:
                
                ClientInterface.isKeepAlive = false;
                        
//                messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                if (audioStreamRecorder == null) {
                      audioStreamRecorder = new AudioStreamRecorder(clientInterfaceFrame.voiceSocket);
//                    recorder.start();
                }
                if (audioStreamPlayer == null) {
                    audioStreamPlayer = new AudioStreamPlayer();
  //                player.start();
                }
                
                displayInGUI.append("\nReconnecting previous call...\n");
                System.out.println("TRANSFER_CALL.TRANSFER_CALL.UNHOLD received");
                
//                confirmationByte = PacketProcessor.makeSignalingPacket(voicechatparser.Constants.TRANSFER_CALL.UNHOLD_CONFIRMATION, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity,clientInterfaceFrame.friendIdentity );
                confirmationByte = parser.makeSignalingPacket(voicechatparser.Constants.TRANSFER_CALL.UNHOLD_CONFIRMATION, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity,clientInterfaceFrame.friendIdentity );
                
                sendSignalingPacket(confirmationByte);
      
                  break;
                
            case voicechatparser.Constants.TRANSFER_CALL.SUCCESS:
//                messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                Constants.MESSAGE_RESEND_MAP.remove(messageDTO.getPacketID() + "_" + voicechatparser.Constants.TRANSFER_CALL.SUCCESS_CONFIRMATION);
                
                displayInGUI.append("\nCall transferred "+clientInterfaceFrame.friendIdentity+" -> "+messageDTO.getUserIdentity());
                
//                confirmationByte = PacketProcessor.makeUnRegisterPacket(voicechatparser.Constants.VOICE_UNREGISTERED, userIdentity);
                confirmationByte = parser.makeUnRegisterPacket(voicechatparser.Constants.VOICE_UNREGISTERED, userIdentity);
                
                clientInterfaceFrame.sendRegisterPacket(confirmationByte);
                
                clientInterfaceFrame.answerButton.setEnabled(false);
                clientInterfaceFrame.busyButton.setEnabled(false);
                clientInterfaceFrame.callButton.setEnabled(true);
                clientInterfaceFrame.byeButton.setEnabled(false);
                clientInterfaceFrame.cancelButton.setEnabled(false);
                clientInterfaceFrame.callButton.setEnabled(false);
                clientInterfaceFrame.sendRegButton.setEnabled(true);
                clientInterfaceFrame.userIdentityInput.setEnabled(true);
                clientInterfaceFrame.friendIdentityInput.setEditable(true);
                clientInterfaceFrame.busyTransferButton.setEnabled(false);
                clientInterfaceFrame.answerTransferButton.setEnabled(false);
                
                //displayInGUI.append("\nCALL TRANSFERED SUCCESSFULLY\n");
                System.out.println("TRANSFER_CALL.SUCCESS received");
                //confirmationByte = PacketProcessor.makeSignalingPacket(voicechatparser.Constants.TRANSFER_CALL.SUCCESS_CONFIRMATION, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity());
                confirmationByte = parser.makeSignalingPacket(voicechatparser.Constants.TRANSFER_CALL.SUCCESS_CONFIRMATION, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getUserIdentity());
                
                sendSignalingPacket(confirmationByte);
                //displayInGUI.append("\nTRANSFER_CALL.SUCCESS_CONFIRMATION "+clientInterfaceFrame.userIdentity+"->"+messageDTO.getFriendIdentity());
                System.out.println("TRANSFER_CALL.SUCCESS_CONFIRMATION "+clientInterfaceFrame.userIdentity+"->"+messageDTO.getUserIdentity());
                Constants.MESSAGE_RESEND_MAP.remove(messageDTO.getPacketID());
                
                break;
                
            case voicechatparser.Constants.TRANSFER_CALL.SUCCESS_CONFIRMATION:
                
                //displayInGUI.append("\nTRANSFER_CALL.SUCCESS_CONFIRMATION received\n");
                System.out.println("TRANSFER_CALL.SUCCESS_CONFIRMATION received");
                
                
                break;
                
            case voicechatparser.Constants.TRANSFER_CALL.CONNECTED:
//                messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                Constants.MESSAGE_RESEND_MAP.remove(messageDTO.getPacketID() + "_" + voicechatparser.Constants.TRANSFER_CALL.CONNECTED_CONFIRMATION);
                //displayInGUI.append("\nTRANSFER_CALL.CONNECTED received\n");
                packetType = voicechatparser.Constants.TRANSFER_CALL.CONNECTED_CONFIRMATION;
                packetID = Constants.getRandromPacketId();
                //confirmationByte = PacketProcessor.makeTransferConnected(packetType, packetID, userIdentity, ClientInterfaceFrame.toTransferFriendIdentity);
                //confirmationByte = PacketProcessor.makeSignalingPacket(packetType, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity());
                
                confirmationByte = parser.makeSignalingPacket(packetType, messageDTO.getPacketID(), clientInterfaceFrame.userIdentity, messageDTO.getUserIdentity());
                
                sendSignalingPacket(confirmationByte);
                System.out.println("TRANSFER_CALL.CONNECTED_CONFIRMATION sent from "+ clientInterfaceFrame.userIdentity+" to "+ messageDTO.getUserIdentity());
                //clientInterfaceFrame.byeButton.setEnabled(false);
                displayInGUI.append("\n"+clientInterfaceFrame.userIdentity+" & "+ messageDTO.getUserIdentity()+" are connected\n");
                
//                packetID = voicechatparser.Constants.getRandromPacketId();
//                confirmationByte = PacketProcessor.makeRegisterPacket(voicechatparser.Constants.VOICE_REGISTER, clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity(), packetID);
//                sendSignalingPacket(confirmationByte);
                
                if (audioStreamRecorder == null) {
                    audioStreamRecorder = new AudioStreamRecorder(clientInterfaceFrame.voiceSocket);
//                  recorder.start();
                }
                if (audioStreamPlayer == null) {
                    audioStreamPlayer = new AudioStreamPlayer();
    //              player.start();
                }
                
                clientInterfaceFrame.isTransferred = true;
                clientInterfaceFrame.friendIdentity = messageDTO.getUserIdentity();
                
                break;
                
            case voicechatparser.Constants.TRANSFER_CALL.CONNECTED_CONFIRMATION:
                clientInterfaceFrame.busyTransferButton.setEnabled(false);
                clientInterfaceFrame.answerTransferButton.setEnabled(false);
                clientInterfaceFrame.byeButton.setEnabled(true);
                
//                messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
                messageDTO = parser.parse(receivedBuffer,0);
                Constants.MESSAGE_RESEND_MAP.remove(messageDTO.getPacketID() + "_" + voicechatparser.Constants.TRANSFER_CALL.CONNECTED_CONFIRMATION);
                //displayInGUI.append("\nTRANSFER_CALL.CONNECTED_CONFIRMATION received\n");
                System.out.println("TRANSFER_CALL.CONNECTED_CONFIRMATION received");
                //clientInterfaceFrame.byeButton.setEnabled(false);
                displayInGUI.append("\n"+clientInterfaceFrame.userIdentity+" & "+ messageDTO.getUserIdentity()+" are connected\n");

//                messageDTO = PacketProcessor.getSignalingPacket(receivedBuffer);
//                packetID = voicechatparser.Constants.getRandromPacketId();
//                confirmationByte = PacketProcessor.makeRegisterPacket(voicechatparser.Constants.VOICE_REGISTER, clientInterfaceFrame.userIdentity, messageDTO.getFriendIdentity(), packetID);
//                sendSignalingPacket(confirmationByte);
                
                if (audioStreamRecorder == null) {
                    audioStreamRecorder = new AudioStreamRecorder(clientInterfaceFrame.voiceSocket);
//                  recorder.start();
                }
                if (audioStreamPlayer == null) {
                    audioStreamPlayer = new AudioStreamPlayer();
    //              player.start();
                }
                clientInterfaceFrame.isTransferred = true;
                clientInterfaceFrame.friendIdentity = messageDTO.getUserIdentity();
                break;
         
        }
        DisableInput();
    }

    public void DisableInput() {
        if (packetType == voicechatparser.Constants.VOICE_REGISTER_CONFIRMATION) {
            clientInterfaceFrame.sendRegButton.setEnabled(false);
            clientInterfaceFrame.sendRegButton.setToolTipText("You Must Leave Chat Before Rejoining");
            clientInterfaceFrame.callButton.setToolTipText("Press to Send Message");
            clientInterfaceFrame.answerButton.setToolTipText("Press to Leave Chat");
            clientInterfaceFrame.callButton.setEnabled(true);
            clientInterfaceFrame.userIdentityInput.setEnabled(false);
            clientInterfaceFrame.friendIdentityInput.setEnabled(false);

        }
    }

    public void sendSignalingPacket(byte[] sendingBytePacket) {
        
        try {
            DatagramPacket finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, InetAddress.getByName(Constants.VOICE_SERVER_IP), Constants.VOICE_BINDING_PORT);
            clientInterfaceFrame.voiceSocket.send(finalPacket);
        } catch (UnknownHostException e) {
            System.err.println(e);
        } catch (IOException e) {
            System.err.println(e);
        }
    }
    
    public void sendKeepAlivePacket(final byte[] sendingBytePacket)
    {
        Thread t = new Thread(){
            public void run()
            {
                while(ClientInterface.isKeepAlive)
                {
                    try {
                        Thread.sleep(1000);
                        DatagramPacket finalPacket = new DatagramPacket(sendingBytePacket, sendingBytePacket.length, InetAddress.getByName(Constants.VOICE_SERVER_IP), Constants.VOICE_BINDING_PORT);
                        clientInterfaceFrame.voiceSocket.send(finalPacket);
                    } catch (UnknownHostException e) {
                        System.err.println(e);
                    } catch (IOException e) {
                        System.err.println(e);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ClientRecieve.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        t.start();
    }
}
