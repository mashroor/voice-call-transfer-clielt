/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.media;

import com.ringid.sound.G729A;
import com.ringid.utils.Constants;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.TargetDataLine;
import voicechatparser.VoiceChatParser;

/**
 *
 * @author Anwar
 */
public class AudioStreamRecorder {

    boolean isRunning = false;
    AudioFormat adFormat;
    TargetDataLine targetDataLine;
    AudioInputStream InputStream;
    DatagramSocket clientSocket;

    public AudioStreamRecorder(DatagramSocket sock) {
        clientSocket = sock;
        captureAudio();
        isRunning = true;
    }

    public void stopRecorder() {
        isRunning = false;
        if (targetDataLine != null) {
            //targetDataLine.drain();
            targetDataLine.flush();
            targetDataLine.close();
        }
    }

    private void captureAudio() {
        try {
            adFormat = getAudioFormat();
            DataLine.Info dataLineInfo = new DataLine.Info(TargetDataLine.class, adFormat);
            targetDataLine = (TargetDataLine) AudioSystem.getLine(dataLineInfo);
            targetDataLine.open(adFormat);
            targetDataLine.start();

            Thread captureThread = new Thread(new CaptureThread());
            captureThread.start();
        } catch (Exception e) {
            System.err.println("Exception-->" + e);
        }
    }

    private AudioFormat getAudioFormat() {
        float sampleRate = 8000.0F;
        int sampleInbits = 16;
        int channels = 1;
        boolean signed = true;
        boolean bigEndian = false;
        return new AudioFormat(sampleRate, sampleInbits, channels, signed, bigEndian);
    }

    class CaptureThread extends Thread {

        @Override
        public void run() {
            int[] packetNumber = {5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
            Random r = new Random();
            try {
                G729A g729 = new G729A();
                InetAddress IPAddress = InetAddress.getByName(Constants.VOICE_SERVER_IP);
                while (isRunning) {
                    int frame_size_in_byte = 10 * 160;
                    byte[] packet_buffer = new byte[(frame_size_in_byte / 160) * 10];
                    byte[] read_bytes = new byte[frame_size_in_byte];
                    int cnt = targetDataLine.read(read_bytes, 0, read_bytes.length);
                    if (cnt > 0) {
                        try {
                            cnt = g729.encode(byteArrayToShortArray(read_bytes), 0, packet_buffer, frame_size_in_byte / 2);
                        } catch (Exception ex) {
                        }
                        byte[] sendingBytes = VoiceChatParser.getInstance().makeVoicePacket(0, packet_buffer);//new byte[packet_buffer.length + 1];
                        
//                        sendingBytes[0] = (byte) 0;
//                        System.arraycopy(packet_buffer, 0, sendingBytes, 1, packet_buffer.length);
                        DatagramPacket sendPacket = new DatagramPacket(sendingBytes, sendingBytes.length, IPAddress, Constants.VOICE_BINDING_PORT);
                        clientSocket.send(sendPacket);
                    }
                }
            } catch (Exception e) {
                System.out.println("CaptureThread::run()" + e);
                System.exit(0);
            }
        }
    }

    public static short[] byteArrayToShortArray(byte[] byteArray) {
        short[] short_array = new short[byteArray.length / 2];
        for (int i = 0; i < short_array.length; i++) {
            short_array[i] = (short) (((int) byteArray[2 * i + 1] << 8) + (int) byteArray[2 * i]);
        }
        return short_array;
    }
}