/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.media;

import com.ringid.sound.G729A;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

/**
 *
 * @author Anwar
 */
public class AudioStreamPlayer implements OnReceiveListner {

    ByteArrayOutputStream byteOutputStream;
    AudioFormat adFormat;
    TargetDataLine targetDataLine;
    AudioInputStream InputStream;
    SourceDataLine sourceLine = null;
    G729A g729;
    boolean isRunning = false;

    public AudioStreamPlayer() {
        g729 = new G729A();
        init();
        isRunning = true;
    }

    public void stopPlayer() {
        isRunning = false;
        if (sourceLine != null) {
            sourceLine.drain();
            sourceLine.close();
        }
    }

    private void init() {
        try {
            adFormat = getAudioFormat();
            DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, adFormat);
            sourceLine = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
            sourceLine.open(adFormat);
            sourceLine.start();
        } catch (LineUnavailableException ex) {
            System.out.println(ex);
        }
    }

    @Override
    public void onReceivedMessage(byte[] receivedByte, int length) {
        try {
            length = length - 1;
            //byte audioData[] = new byte[length];
            //System.arraycopy(receivedByte, 1, audioData, 0, length);

            short[] decoded = new short[(length / 10) * 80];
            //int ret = g729.decode(audioData, decoded, length);
            int ret = g729.decode(receivedByte, decoded, length);
            byte[] playBytes = shortArrayToByteArray(decoded);
            if (sourceLine == null) {
                isRunning = true;
                adFormat = getAudioFormat();
                DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, adFormat);
                sourceLine = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
                sourceLine.open(adFormat);
                sourceLine.start();
            } else if (sourceLine != null && !sourceLine.isRunning()) {
                sourceLine.start();
            }
            InputStream byteInputStream = new ByteArrayInputStream(playBytes);
            InputStream = new AudioInputStream(byteInputStream, adFormat, playBytes.length / adFormat.getFrameSize());

            Thread playThread = new Thread(new PlayThread());
            playThread.start();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private AudioFormat getAudioFormat() {
        float sampleRate = 8000.0F;
        int sampleInbits = 16;
        int channels = 1;
        boolean signed = true;
        boolean bigEndian = false;
        return new AudioFormat(sampleRate, sampleInbits, channels, signed, bigEndian);
    }

    class PlayThread extends Thread {

        byte tempBuffer[] = new byte[10000];

        @Override
        public void run() {
            try {
                int cnt;
                while ((cnt = InputStream.read(tempBuffer, 0, tempBuffer.length)) != -1) {
                    if (cnt > 0) {
                        sourceLine.write(tempBuffer, 0, cnt);
                    }
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    public static byte[] shortArrayToByteArray(short[] short_array) {
        byte[] byte_array = new byte[short_array.length * 2];
        for (int i = 0; i < short_array.length; ++i) {
            byte_array[2 * i] = getByte1(short_array[i]);
            byte_array[2 * i + 1] = getByte2(short_array[i]);
        }
        return byte_array;
    }

    public static byte getByte1(short s) {
        return (byte) s;
    }

    public static byte getByte2(short s) {
        return (byte) (s >> 8);
    }
}
