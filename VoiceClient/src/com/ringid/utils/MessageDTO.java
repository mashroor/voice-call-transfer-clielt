///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.ringid.utils;
//
//import java.net.InetAddress;
//
///**
// *
// * @author Anwar
// */
//public class MessageDTO {
//
//    private int packetType;
//    private String packetID;
//    private long userIdentity;
//    private long friendIdentity;
//    private long transferedUserIdentity;////////////////////////////////////////////////////////////////////////////////
//    private int voiceBindingPort;
//    private int voiceCommunicationPort;
//    private InetAddress inetAddress;
//    private int mapPort;
//    private String mapAddress="";
//    private byte[] voiceData;
//
//    public MessageDTO() {
//    }
//
//    public int getPacketType() {
//        return packetType;
//    }
//
//    public void setPacketType(int packetType) {
//        this.packetType = packetType;
//    }
//
//    public String getPacketID() {
//        return packetID;
//    }
//
//    public void setPacketID(String packetID) {
//        this.packetID = packetID;
//    }
//
//    public long getUserIdentity() {
//        return userIdentity;
//    }
//
//    public void setUserIdentity(long userIdentity) {
//        this.userIdentity = userIdentity;
//    }
//    
//    
//    /////////////////////////////////////////////////////////////////////////////////////////////////////////
//    public long getTransferedUserIdentity() {
//        return transferedUserIdentity;
//    }
//
//    public void setTransferedUserIdentity(long transferedUserIdentity) {
//        this.transferedUserIdentity = transferedUserIdentity;
//    }
//    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    
//    
//    public void setMapAddress(String var)
//    {
//        mapAddress = var;
//    }
//    
//    public String getMapAddress()
//    {
//        return mapAddress;
//    }
//    
//
//    public long getFriendIdentity() {
//        return friendIdentity;
//    }
//
//    public void setFriendIdentity(long friendIdentity) {
//        this.friendIdentity = friendIdentity;
//    }
//    
//    public int getVoiceBindingPort() {
//        return voiceBindingPort;
//    }
//
//    public void setVoiceBindingPort(int voiceBindingPort) {
//        this.voiceBindingPort = voiceBindingPort;
//    }
//    
//    
//    
//    public int getVoiceCommunicationPort() {
//        return voiceCommunicationPort;
//    }
//
//    public void setVoiceCommunicationPort(int voiceCommunicationPort) {
//        this.voiceCommunicationPort = voiceCommunicationPort;
//    }
//    
//    
//    
//
//    public InetAddress getInetAddress() {
//        return inetAddress;
//    }
//
//    public void setInetAddress(InetAddress inetAddress) {
//        this.inetAddress = inetAddress;
//    }
//
//    public int getMapPort() {
//        return mapPort;
//    }
//
//    public void setMapPort(int mapPort) {
//        this.mapPort = mapPort;
//    }
//    
//    public byte[] getVoiceData()
//    {
//        return voiceData;
//    }
//    
//    public void setVoiceData(byte [] voiceData)
//    {
//        this.voiceData = voiceData;
//    }
//    
//    
//}
