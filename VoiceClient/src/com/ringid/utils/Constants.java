/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.utils;

import com.ringid.voiceclient.ResendPacketDTO;
import java.security.SecureRandom;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Anwar
 */
public class Constants {

    public static String VOICE_SERVER_IP = "38.127.68.57"; //"192.168.1.125"; //"104.193.36.132";//"104.193.36.133";//"104.193.36.133";//"38.108.92.154";//
    public static int VOICE_REGISTER_PORT = 1250;
    public static int VOICE_BINDING_PORT = 1500;
//    public static final int VOICE_MEDIA = 0;
//    public static final int VOICE_REGISTER = 1;
//    public static final int VOICE_UNREGISTERED = 2;
//    public static final int VOICE_REGISTER_CONFIRMATION = 3;
    public static Map<String, ResendPacketDTO> MESSAGE_RESEND_MAP = new ConcurrentHashMap<String, ResendPacketDTO>();
    public static final int NUMBER_OF_RESEND = 5;
    public static final int REGISTER_RESEND_TIME = 30000;
    public static String CALL_ID = "";
    public static String TRANSFER_CALL_ID = "";
    
//
//    public class CALL_STATE {
//
//        public final static byte KEEPALIVE = 4;
//        public final static byte CALLING = 5;
//        public final static byte RINGING = 6;
//        public final static byte IN_CALL = 7;
//        public final static byte ANSWER = 8;
//        public final static byte BUSY = 9;
//        public final static byte CANCELED = 10;
//        public final static byte CONNECTED = 11;
//        public final static byte DISCONNECTED = 12;
//        public final static byte BYE = 13;
//        public final static byte IDEL = 14;
//        public final static byte NO_ANSWER = 15;
//        public final static byte USER_AVAILABLE = 16;
//        public final static byte USER_NOT_AVAILABLE = 17;
//    };
//    
//    public class TRANSFER_CALL {/////////////////////////////////////////////////////////////////////
//
//        public static final byte HOLD = 28;
//        public static final byte HOLD_CONFIRMATION = 29;
//        public static final byte BUSY = 30;
//        public static final byte BUSY_CONFIRMATION = 31;
//        public static final byte SUCCESS = 32;
//        public static final byte SUCCESS_CONFIRMATION = 33;
//        public static final byte CONNECTED = 34;
//        public static final byte CONNECTED_CONFIRMATION = 35;
//        public static final byte UNHOLD = 36;
//        public static final byte UNHOLD_CONFIRMATION = 37;
//        public static final byte TRANSFER = 105;
//        public static final byte TRANSFER_CONFIRMATION = 106;
//    }/////////////////////////////////////////////////////////////////////////////////////////////////
//    
//    
//    public static final int VOICE_REGISTER_PUSH = 20;
//    public static final int VOICE_REGISTER_PUSH_CONFIRMATION = 21;
//    public final static byte VOICE_ADDRESS_REQUEST = 100;
//    public final static byte VOICE_ADDRESS = 101;

    public static String getRandromPacketId() {
        SecureRandom random = new SecureRandom();
        char[] chars = new char[8];
        for (int i = 0; i < chars.length; i++) {
            int v = random.nextInt(10 + 26 + 26);
            char c;
            if (v < 10) {
                c = (char) ('0' + v);
            } else if (v < 36) {
                c = (char) ('a' - 10 + v);
            } else {
                c = (char) ('A' - 36 + v);
            }
            chars[i] = c;
        }

        String key = new String(chars);
        return key + "" + System.currentTimeMillis();
    }
}
